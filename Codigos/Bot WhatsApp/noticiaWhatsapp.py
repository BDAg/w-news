from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from bs4 import BeautifulSoup
from lxml import html
from selenium import webdriver
from datetime import datetime
import selenium
import random
import time
import pymongo


conexao = pymongo.MongoClient("mongodb+srv://admin:admin@cluster0-krymp.mongodb.net/test?retryWrites=true")
mydb = conexao['Wnews']


def verificarMensagem():

    a = ActionChains(driver)
    a.key_down(Keys.SHIFT).send_keys(Keys.ENTER).key_up(Keys.SHIFT)

    conversaNova = driver.find_elements_by_class_name('_2WP9Q')

    for conversa in conversaNova:

        conversaTratada = conversa.get_attribute("outerHTML")
        usuario = conversa.find_element_by_class_name('_19RFN').text

        if usuario == 'WNews':

            pass

        else:

            if 'P6z4j' in conversaTratada:

                conversa.click()
                caixaDeTexto = driver.find_element_by_xpath('//*[@id="main"]/footer/div[1]/div[2]/div/div[2]')
                time.sleep(2)

                if '$$' in conversaTratada:

                    caixaDeTexto.send_keys('Entraremos em contato com você o mais rápido possível!')
                    caixaDeTexto.send_keys(Keys.ENTER)
                    time.sleep(10)

                    ajudarUsuario = driver.find_element_by_xpath('//*[@id="pane-side"]/div/div/div/div[1]/div/div/div[2]')
                    ajudarUsuario.click()

                    caixaDeTexto = driver.find_element_by_xpath('//*[@id="main"]/footer/div[1]/div[2]/div/div[2]')

                    caixaDeTexto.send_keys('O usuario '+usuario+' necessita de ajuda')
                    caixaDeTexto.send_keys(Keys.ENTER)
                    time.sleep(10)

                else:

                    caixaDeTexto.send_keys('Olá, bem vindo ao Whats news!')
                    a.perform()
                    caixaDeTexto.send_keys('Este projeto tem como intuito enviar notícias do G1 aos usuários cadastrados.')
                    a.perform()
                    caixaDeTexto.send_keys('Não perca tempo e cadastre-se já! acesse: https://w-news.herokuapp.com/user/cadastro')
                    a.perform()
                    a.perform()
                    caixaDeTexto.send_keys('Esta é uma mensagem automática, se deseja entrar em contato com um de nossos desenvolvedores, envie "$$" na próxima mensagem e aguarde nosso contato!')
                    caixaDeTexto.send_keys(Keys.ENTER)
                    time.sleep(10)

                    reset = driver.find_element_by_xpath('//*[@id="pane-side"]/div[1]/div/div/div[1]/div/div/div[2]')
                    reset.click()


def abreChrome():
    
    global driver

    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("user-data-dir=selenium") 
    prefs = {"profile.default_content_setting_values.notifications" : 2}
    chrome_options.add_experimental_option("prefs",prefs)
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.get('https://web.whatsapp.com')

    while True:

        try:
            verificacao = driver.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[1]/div/div[1]/div').text

        except:
            verificacao = ''

        if str(verificacao) == 'Para usar o WhatsApp no seu computador:' or str(verificacao) == 'To use WhatsApp on your computer:':

            print('Faça a conexão com o whatsapp web')
            time.sleep(5)

        else:
            
            break


def enviaNoticia(numero,id):

    linhaNoticia = mydb.noticias.find_one({'_id':id})
    conteudoNoticia = linhaNoticia['materiaNoticia']
    tituloNoticia = linhaNoticia['tituloNoticia']
    linkNoticia = linhaNoticia['linkNoticia']
    dataNoticia = linhaNoticia['dataNoticia']
    categoriaNoticia = linhaNoticia['categoriaNoticia']
    
    a = ActionChains(driver)
    a.key_down(Keys.SHIFT).send_keys(Keys.ENTER).key_up(Keys.SHIFT)

    caixaDeTexto = driver.find_element_by_xpath('//*[@id="main"]/footer/div[1]/div[2]/div/div[2]')

    caixaDeTexto.send_keys(categoriaNoticia.upper())
    a.perform()
    a.perform()
    caixaDeTexto.send_keys(tituloNoticia)
    a.perform()
    a.perform()
    caixaDeTexto.send_keys(conteudoNoticia)
    a.perform()
    a.perform()
    caixaDeTexto.send_keys(dataNoticia)
    a.perform()
    caixaDeTexto.send_keys(linkNoticia)
    caixaDeTexto.send_keys(Keys.ENTER)
    time.sleep(10)

    mydb.noticias.update_one({'_id' : id}, {'$push':{'usuariosEnviados' : numero }})


def buscaNoticia(numero, categoria):

    dataAtual = datetime.now().date()
    dataAtual= str(dataAtual).split('-') 
    data = dataAtual[2]+'/'+dataAtual[1]+'/'+dataAtual[0]

    verificarMensagem()

    driver.get('https://web.whatsapp.com/send?phone='+numero)
    time.sleep(5)

    for c in categoria:

        print(c)
        noticia = mydb.noticias.find({ 'categoriaNoticia' : c })

        for n in noticia:

            if (numero not in n['usuariosEnviados']) and (data == n['dataNoticia']):

                enviaNoticia(numero,n['_id'])
                break

            else:

                pass


    reset = driver.find_element_by_xpath('//*[@id="pane-side"]/div[1]/div/div/div[1]/div/div/div[2]')
    reset.click()


abreChrome()

horaAtual = datetime.now().time()
horaAtual= str(horaAtual).split(':') 
hora = horaAtual[0]

if int(hora) <= 6:
    auxiliar = '06'

elif int(hora) > 6 and int(hora) <= 12:
    auxiliar = '12'

elif int(hora) > 12 and int(hora) <= 18:
    auxiliar = '18'

else:
    auxiliar = '06'

while True:

    # entrar no for somente quando for a hora
    horaAtual = datetime.now().time()
    horaAtual= str(horaAtual).split(':') 
    hora = horaAtual[0]
   
    Usuario = mydb.usuarios.find({})
    
    time.sleep(10)
    verificarMensagem()
    print(auxiliar)

    if ((str(hora)=='06')or(str(hora)=='12')or(str(hora)=='18')):

        if hora == auxiliar:

            for i in Usuario:

                if str(hora) in i['horario']:

                    buscaNoticia(i['numero'],i['categoria'])

        if hora == '06':
            auxiliar = '12'

        elif hora == '12':
            auxiliar = '18'

        elif hora == '18':
            auxiliar = '06'
    
    time.sleep(2)

    print('sem mensagens')

driver.quit()