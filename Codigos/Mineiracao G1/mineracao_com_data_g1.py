import requests
from bs4 import BeautifulSoup
from datetime import datetime
import pymongo

conexao = pymongo.MongoClient("mongodb+srv://admin:admin@cluster0-krymp.mongodb.net/test?retryWrites=true")
mydb = conexao['Wnews']

global VTUDO
VTUDO = []


def mandaDado ():
    for c in VTUDO:

        tituloRepetido = mydb.noticias.find_one({"tituloNoticia": c["titulo"]})
    
        if tituloRepetido:
            print('Noticia Repetida')

        else:
            mydb.noticias.insert({

                "categoriaNoticia": c["categoria"],
                "tituloNoticia": c["titulo"],
                "materiaNoticia": c["materia"],
                "dataNoticia": c["data"],
                "linkNoticia": c["link"],
                "usuariosEnviados": []
            })

def coletaG1(s, complementoLink):

    linkn = s.find_all ("a", {"class": "feed-post-link gui-color-primary gui-color-hover"})


    for l in linkn:
        
        r = requests.get(l['href'])
        s = BeautifulSoup(r.content,'html.parser')
        datanoticia = s.find("time", {"itemprop": "datePublished"})
        datanoticia = datanoticia.text.split()
        datanoticia = datanoticia[0]

        # categoria = s.find ("div", {"class": "header-title-content"})

        titulo = s.find("h1", {"class": "content-head__title"})
        materia = s.find_all("p", {"class": "content-text__container"})
        dataAtual = datetime.now().date()
        dataAtual= str(dataAtual).split('-') 
        data = dataAtual[2]+'/'+dataAtual[1]+'/'+dataAtual[0] 
        linha = ''

        for texto in materia:
            linha += texto.text


        if '/' in complementoLink:

            categoria = complementoLink.split('/')
            categoria = categoria[-1]

        else:

            categoria = complementoLink


        if str(data) == str(datanoticia):
            print('\n========================================\n')
            print("\n\n",categoria, "\n\n" ,titulo.text, "\n\n" ,linha, "\n\n" ,data, "\n\n",l['href'])
            tudo = {"categoria": categoria, "titulo": titulo.text, "materia": linha, "data": data, "link": l['href']}
            
            VTUDO.append(tudo)
        else:
            print("\n sem mais noticias dessa categoria")
            break
                



def escolhaDoSite():

    vlinks = ["carros","economia/agronegocios","economia","economia/tecnologia"]

    for complementoLink in vlinks:

        r =requests.get("https://g1.globo.com/"+complementoLink)
        s = BeautifulSoup(r.content,'html.parser')
        coletaG1(s, complementoLink)


while True:

    escolhaDoSite()
    mandaDado()
