# Whats News
Um grupo formado na FATEC Pompeia em Fevereiro de 2019, com o objetivo de automatizar o envio de noticias via WhatsApp.


[Wiki do Projeto](https://gitlab.com/BDAg/w-news/wikis/Home)


## Pesquisas:
-[Banco de dados](https://gitlab.com/BDAg/w-news/wikis/Banco-de-dados)


-[Web Crawler](https://gitlab.com/BDAg/w-news/wikis/Web-Crawler)


-[Biblioteca Selenium](https://gitlab.com/BDAg/w-news/wikis/Selenium)

## Equipe
- [Gustavo Andrade (SM)](https://gitlab.com/BDAg/w-news/wikis/Gustavo-Andrade-(SM))<b>
- [Willi Kevin (SM)](https://gitlab.com/BDAg/w-news/wikis/Willi-Kevin)<b>
- [Fernanda Miranda](https://gitlab.com/BDAg/w-news/wikis/Fernanda-Miranda)<b>
- [Bruno Shimura](https://gitlab.com/BDAg/w-news/wikis/Bruno-Shimura)<b>
- [Daniel Vieira](https://gitlab.com/BDAg/w-news/wikis/Daniel-Vieira)<b>
- [Natan Prado](https://gitlab.com/BDAg/w-news/wikis/Natan-Prado)<b>
- [Emerson Garcia](https://gitlab.com/BDAg/w-news/wikis/Emerson-Garcia)<b>
- [Fernando Augusto](https://gitlab.com/BDAg/w-news/wikis/Fernando-Augusto)<b>
- [Breno Ponciano](https://gitlab.com/BDAg/w-news/wikis/Breno-Ponciano)<b>
- [Milene Silva](https://gitlab.com/BDAg/w-news/wikis/Milene-Silva)<b>
- [Luis Fernando](https://gitlab.com/BDAg/w-news/wikis/Luis-Fernando)<b>